#ifndef __PARAMS__LRR__
#define __PARAMS__LRR__

class LRR;


#include "params/ReuseSetAssoc.hh"

struct LRRParams
    : public ReuseSetAssocParams
{
    LRR * create();
};

#endif // __PARAMS__LRR__
