#ifndef __PARAMS__ReuseTags__
#define __PARAMS__ReuseTags__

class ReuseTags;

#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"

#include "params/ClockedObject.hh"

struct ReuseTagsParams
    : public ClockedObjectParams
{
    uint64_t size;
    int block_size;
    Cycles hit_latency;
};

#endif // __PARAMS__ReuseTags__
