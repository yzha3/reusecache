#ifndef __PARAMS__ReuseCache__
#define __PARAMS__ReuseCache__

class ReuseCache;

#include <cstddef>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "params/ReuseTags.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "params/System.hh"
#include <cstddef>
#include "params/ReusePrefetcher.hh"
#include <cstddef>
#include <vector>
#include "base/types.hh"
#include "base/addr_range.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include <cstddef>
#include "base/types.hh"

#include "params/MemObject.hh"

struct ReuseCacheParams
    : public MemObjectParams
{
    ReuseCache * create();
    bool prefetch_on_access;
    Cycles hit_latency;
    ReuseTags * tags;
    int tgts_per_mshr;
    System * system;
    ReusePrefetcher * prefetcher;
    bool sequential_access;
    std::vector< AddrRange > addr_ranges;
    Counter max_miss_count;
    int write_buffers;
    Cycles response_latency;
    bool is_top_level;
    int assoc;
    bool two_queue;
    int mshrs;
    bool forward_snoops;
    uint64_t size;
    unsigned int port_mem_side_connection_count;
    unsigned int port_cpu_side_connection_count;
};

#endif // __PARAMS__ReuseCache__
