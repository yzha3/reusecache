#ifndef __PARAMS__ReuseSetAssoc__
#define __PARAMS__ReuseSetAssoc__

class ReuseSetAssoc;

#include <cstddef>
#include <cstddef>
#include "base/types.hh"

#include "params/ReuseTags.hh"

struct ReuseSetAssocParams
    : public ReuseTagsParams
{
    bool sequential_access;
    int assoc;
};

#endif // __PARAMS__ReuseSetAssoc__
