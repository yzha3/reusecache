Reuse Cache on gem5
==========================================
Implementation of the reuse cache

* For level 2 cache

* For single core system

* No prefetcher, test on X86 ISA

* For stable version, `gem5-stable-0e86fac7254c`

Usage:

* copy `cache/` folder into `src/mem/`, several files may be replaced

* If you have build X86 before, then copy the files in `header/` into `build/X86/params`

* compile and build the simulator, `scons PROTOCOL=MESI_Two_Level build/X86/gem5.opt`

Replace Files:

* `blk.hh` 

	- add two new variable in the `Class CacheBlk`:   `int reuse` and `uint8_t bypass_data`
	- modify `const CacheBlk& operator=(const CacheBlk& rhs)`, add `reuse = rhs.reuse`

* `cache/SConscript` 

	- add `SimObject('reuse-base.cc')`, `Source('reuse-base.cc')` and `Souce('reuse-cache.cc')`

* `cache/tags/SConscript` 

	- add `Source('lrr.cc')`, `Source('reuse-base.cc')` and `Source('reuse_set_assoc.cc')`

* `cache/tags/Tags.py`

	- add definations for `class ReuseTags`, `class ReuseSetAssoc`, `class LRR`

* `cache/prefetch/SConscript`
	
	- add `Source('reuse-base.cc')`

* `cache/prefetcher/Prefetcher.py`

	- add defination for `class ReusePrefetcher` 

Possible solution for compile error information related to the `reuse-cache_impl.hh`:

* `cd src/mem/cache/`

* `cp cache_impl.hh reuse-cache_impl.hh`

* modify the new `reuse-cache_impl.hh`

	- `#include mem/cache/prefetch/base.hh` into `#include mem/cache/prefetch/reuse-base.hh`
	- `#include mem/cache/cache.hh` into `#include mem/cache/reuse-cache.hh`
	- replace every `BaseCache` into `ReuseCache`
	- `#ifndef __MEM_CACHE_CACHE_IMPL_HH__` into `#ifndef __MEM_REUSE_CACHE_IMPL_HH__`
	- `#define __MEM_CACHE_CAHCE_IMPL_HH__` into `#define __MEM_REUSE_CACHE_IMPL_HH__`
	- in the function `Cache<TagStore>::handleFill()`, add following codes just before return
~~~~
if (blk->bypass_data!=NULL){
	std::memcpy(blk->data, blk->bypass_data, blkSize);
	delete[] blk->bypass_data;
	blk->bypass_data = NULL;
	blk->status |= BlkDirty;
}
~~~~

* do not replace files in the `cache/`, modify these files based on previous details

Reference:

1. [Reuse Cache](http://dl.acm.org/citation.cfm?id=2540735)

2. [gem5](http://www.gem5.org/Main_Page)