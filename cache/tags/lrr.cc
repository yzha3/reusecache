/*
 * Copyright (c) 2012-2013 ARM Limited
 * All rights reserved.
 *
 * The license below extends only to copyright in the software and shall
 * not be construed as granting a license to any other intellectual
 * property including but not limited to intellectual property relating
 * to a hardware implementation of the functionality of the software
 * licensed hereunder.  You may use the software subject to the license
 * terms below provided that you ensure that this notice is replicated
 * unmodified and in its entirety in all distributions of the software,
 * modified or unmodified, in source code or in binary form.
 *
 * Copyright (c) 2003-2005,2014 The Regents of The University of Michigan
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Erik Hallnor
 */

/**
 * @file
 * Definitions of a LRR tag store.
 */

#include "debug/CacheRepl.hh"
#include "mem/cache/tags/lrr.hh"
#include "mem/cache/reuse-base.hh"

LRR::LRR(const Params *p)
    : ReuseSetAssoc(p)
{  
	int ratio=1;
	printf("\n**************Use Reuse Cache**************\n\nResue Ratio=");
	scanf("%d",&ratio);
	printf("Miss time before storing data = ");
	scanf("%d",&miss_time_before_store);
	if (miss_time_before_store<1)
	{
		printf("Invalid input for miss time, set it to 1\n");
		miss_time_before_store = 1;
	}
	DTableSize=p->size/(ratio*p->block_size);
	int temp=DTableSize*p->block_size/1024;
	printf("\n******************Summary******************\n\nRatio=%d\tMiss times=%d\nData Array size=%d%sB\tData Blocks=%d\n\n*******************************************\n\n",ratio,miss_time_before_store,(temp>1023)?(temp/1024):temp,(temp>1023)?"M":"k",DTableSize);
	DSet=new SetType;
	DSet->blks=new BlkType*[DTableSize];
	DSet->assoc=DTableSize;
	DTable=new BlkType[DTableSize];
	dataDTable=new uint8_t[DTableSize*blkSize];
 	for (unsigned i=0;i<DTableSize;++i)
   	{
		BlkType *blk=&DTable[i];
		blk->data=&dataDTable[blkSize*i];
		blk->bypass_data=NULL;
		blk->invalidate();
		blk->whenReady=0;
		blk->isTouched=false;
		blk->size=blkSize;
		blk->set=-1;
		blk->tag=-1;
		blk->reuse=-1;
		DSet->blks[i]=blk;
	}
} 
LRR::~LRR()
{  
	delete [] dataDTable;
	delete [] DTable;
	delete DSet;
}


ReuseSetAssoc::BlkType*
LRR::accessBlock(Addr addr, bool is_secure, Cycles &lat, int master_id)
{ 
	BlkType *blk2= MyfindBlk(addr, is_secure);
	if (blk2 == NULL) return NULL;
		
	BlkType *blk = ReuseSetAssoc::accessBlock(addr, is_secure, lat, master_id);
	assert(blk!=NULL);
	assert(blk->reuse > 0);// reuse must large than 0

	blk->reuse++;
	sets[blk->set].moveToHead(blk);
	DSet->moveToHead(blk2);
	return DSet->blks[0];
}   

ReuseSetAssoc::BlkType*
LRR::findVictim(Addr addr)
{ 
	int  set=extractSet(addr);
	Addr tag=extractTag(addr);
	BlkType *blk=NULL;

	for (unsigned i=0;i<assoc;i++)
 		if (sets[set].blks[i]->tag==tag && sets[set].blks[i]->isValid())
 		{ 
			blk=sets[set].blks[i];
			break;
		}
	//not in this cache
	if (blk==NULL)
 	{ 
		//Find victim
		if (sets[set].blks[assoc-1]->reuse < 0)	
		{
			assert(sets[set].blks[assoc-1]->srcMasterId < cache->system->maxMasters());
			//ReuseSetAssoc::invalidate(sets[set].blks[assoc-1]);
			sets[set].blks[assoc-1]->invalidate();
		}
		sets[set].blks[assoc-1]->reuse=0;
		int i = MymoveToHead(sets[set].blks[assoc-1]);
		blk=sets[set].blks[i];

		if (blk->isValid())
 	 	{ 
			BlkType * blk2=MyfindBlk(regenerateBlkAddr(blk->tag, blk->set), blk->isSecure());
			if ( blk2 != NULL)
 	 	 	{
				assert(blk->bypass_data == NULL && blk2->bypass_data == NULL);

				CopyBlk(blk, blk2);
				assert(blk2->srcMasterId < cache->system->maxMasters());
			//	ReuseSetAssoc::invalidate(blk2);
				blk2->invalidate();
				DSet->moveToTail(blk2);

				return blk;
			}
			if (!blk->isDirty()) 
	 		{
				assert(blk->srcMasterId < cache->system->maxMasters());
				//ReuseSetAssoc::invalidate(blk);
				blk->invalidate();
			}
		}
		return blk;
	}
	else if (blk->reuse < miss_time_before_store-1 && -blk->reuse < miss_time_before_store -1)
	{
		if (blk->reuse < 0)
			blk->reuse--;
		else
			blk->reuse++;
		if (blk->isDirty())
		{
			assert (blk->bypass_data == NULL);

			blk->bypass_data = new uint8_t[blkSize];
			std::memcpy(blk->bypass_data, blk->data, blkSize);
		}
		//ReuseSetAssoc::invalidate(blk);
		blk->invalidate();
		int i = MymoveToHead(blk);
		return sets[set].blks[i];
	}
	else  // find block in cache, second access time to this data
	{ 
		sets[set].moveToHead(blk);//it is a reuse block, so move it to the head directly
		if (sets[set].blks[0]->reuse < 0 ) sets[set].blks[0]->reuse = 0 - sets[set].blks[0]->reuse;
		sets[set].blks[0]->reuse++;
		assert(sets[set].blks[0]->srcMasterId < cache->system->maxMasters());
		//ReuseSetAssoc::invalidate(sets[set].blks[0]);
		//move to head
		DSet->moveToHead(DSet->blks[DTableSize-1]);

		if (DSet->blks[0]->isValid()) 
		{
			BlkType *blk2 = sets[DSet->blks[0]->set].findBlk(DSet->blks[0]->tag, DSet->blks[0]->isSecure());

			assert (blk2 != NULL);      //should find one block
			assert (!blk2->isDirty());
			assert (blk2->reuse > 0);

			blk2->reuse = 0 - blk2->reuse;
			MymoveToHead(blk2);
	 	}

		if (sets[set].blks[0]->isDirty()){

			assert (sets[set].blks[0]->reuse == miss_time_before_store);
			assert (DSet->blks[0]->bypass_data == NULL);

			sets[set].blks[0]->status&=(~BlkDirty);
			DSet->blks[0]->bypass_data=new uint8_t[blkSize];
			std::memcpy(DSet->blks[0]->bypass_data,sets[set].blks[0]->data,blkSize);
	 	}
		return DSet->blks[0];
	} 
}

ReuseSetAssoc::BlkType*
LRR::findBlock(Addr addr,bool is_secure) const
{
	return MyfindBlk(addr, is_secure);
} 

LRR*
LRRParams::create()
{
    return new LRR(this);
}

int
LRR::MymoveToHead(BlkType *blk)
{
	int i=assoc-1, set=blk->set;
	sets[blk->set].moveToHead(blk);
	while (sets[set].blks[i]->reuse<miss_time_before_store && i>0)
		i--;
	for (int j=0;j<i;j++)
		sets[set].moveToHead(sets[set].blks[i]);
	return i;
}

ReuseSetAssoc::BlkType*
LRR::MyfindBlk(Addr addr, bool is_secure) const
{ 
	int set = extractSet(addr);
	Addr tag = extractTag(addr);
	for (int i=0; i<DTableSize; i++)
		if (DSet->blks[i]->tag == tag && DSet->blks[i]->set == set && 
			DSet->blks[i]->isSecure() == is_secure && DSet->blks[i]->isValid())
			return DSet->blks[i];
	return NULL;
}

void
LRR::CopyBlk(BlkType *dst, BlkType *src)
{
	dst->asid   = src->asid;
	dst->tag    = src->tag;
	dst->size   = src->size;
	dst->status = src->status;
	dst->whenReady = src->whenReady;
	dst->set    = src->set;
	dst->refCount = src->refCount;
	dst->task_id = src->task_id;
	std::memcpy(dst->data, src->data, blkSize);
}

void
LRR::invalidate(BlkType *blk)
{
	BlkType *blk1=MyfindBlk(regenerateBlkAddr(blk->tag,blk->set), blk->isSecure());
	BlkType *blk2=sets[blk->set].findBlk(blk->tag,blk->isSecure());
	if (blk1 == blk)
	{
		assert(blk2!=NULL);
		ReuseSetAssoc::invalidate(blk1);
		DSet->moveToTail(blk1);
		blk2->invalidate();
		sets[blk->set].moveToTail(blk2);
	}
	else
	{
		assert(blk1==NULL && blk2!=NULL);
		ReuseSetAssoc::invalidate(blk);
	}
	unsigned int set=blk2->set;
	sets[set].moveToTail(blk2);

}

